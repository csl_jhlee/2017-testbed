package csl.cau.ac.kr.macrobenchmarks2017;

import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.Locale;

/**
 * Created by user on 2017-10-24.
 */

public class VideoDecoderActivity extends AppCompatActivity implements SurfaceHolder.Callback {

    private VideoDecoderThread mVideoDecoder;
    private static final String FILE_PATH = Environment.getExternalStorageDirectory() + "/Download/videouhd.mkv";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //SurfaceView surfaceView = new SurfaceView(this);

        setContentView(R.layout.activity_video);

        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        surfaceView.getHolder().addCallback(this);

        mVideoDecoder = new VideoDecoderThread();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,	int height) {
        if (mVideoDecoder != null) {
            if (mVideoDecoder.init(holder.getSurface(), (TextView) findViewById(R.id.textView), FILE_PATH)) {
                mVideoDecoder.start();
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            mVideoDecoder.join();
                            finish();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            } else {
                mVideoDecoder = null;
            }
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (mVideoDecoder != null) {
            mVideoDecoder.close();
        }
    }


}
