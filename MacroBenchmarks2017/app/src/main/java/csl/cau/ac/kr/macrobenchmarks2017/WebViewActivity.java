package csl.cau.ac.kr.macrobenchmarks2017;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by user on 2017-10-24.
 */

public class WebViewActivity extends AppCompatActivity {
    private WebView mWebView;

    private int mCount = 0;
    private final String[] urls = {
            "http://www.naver.com/",
            "https://www.washingtonpost.com/",
            "https://www.washingtonpost.com/national/small-montana-firm-lands-puerto-ricos-biggest-contract-to-get-the-power-back-on/2017/10/23/31cccc3e-b4d6-11e7-9e58-e6288544af98_story.html",
            "https://paid.outbrain.com/network/redir?p=5sKpgyZihjXk18B5EZMeH_gG4JBuESfqGd_5mcPoXImLNVO8aMn64K68YzGOQbf_ZFS35h58Kkil-FiALJW6haR0pia3B62i4VctimvXZ1dEnyJ8z2FnSownkv9BM-2jmEbQ2AcbFYgmrvcGV6dAWCPgp4hkBl6gf34pkKMksTZY2H1utLNzlx-wEkGidYU5yeG8QwChEHoy1NM9g6Y7qHmEUxuN-PtO5kcTRo9nSjMkESdQ92h9vqdntRqK1C78RoauBgAOBVobv_HQNbyW7FJIBDxlqzbF8Exxw6fvAqCrs301PQC4qZhX6Iv4sTH4P6ixR21mfxdiicRPDzHjyNg004rLQe8PyJ_EpywVox_QjcPHIBm8IgtyNJG9IKYCf0h5q7YnLtovbju5Z_5ew-isqBgt9gQEXrno8lX_H8cXNga0VTLXiwy1EkTHpA0OMSOmQ1-jzemdxsJ-YLru9mdhX-Nqg1MxJs_W3UNZkIOur4xOcWy0OdfnClIyixyXJ6B4Pyz3Pf5dOO122PpU6mIGJdqkqozLwE064Zd5bHkxC3Db_yno-bdRzaFiWQ1RYKGN6Z2Blcfp5J0m0KK1_FEuG1WQpf8TlMfJxQ34XdCODOzff3GBmkVCf9nyQMy1lv52hDNE4vI7JdNlhVji2qi7EpoxPIawlssreQna0OVjb9Fsld5qvyCPkTZXSpjqINIEdh_seVWBh2tiyLccTDrExxSgjI2DARRw5jJuyel3QF-NFqZcJcTI-3B-SiPlpWveLQrN3XRp3wQU25ga5K0iPh7rwYZXzC4jMY88ltk&c=b83dec34&v=3"
    };

    public void wvConfiguration(WebView wv) {
        WebSettings settings = wv.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setBuiltInZoomControls(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setDisplayZoomControls(false);
        settings.setSupportZoom(false);
        settings.setDefaultTextEncodingName("UTF-8");

        settings.setDatabaseEnabled(true);
        settings.setDomStorageEnabled(true);

        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
    }


    private String sc = "(function(){return window.performance.timing.responseEnd;})();";

    public WebViewClient getWvc() {
        final String sc = "(function(){" +
                "var t = window.performance.timing;" +
                "return t.loadEventEnd - t.responseEnd;" +
                "})()";

        return new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mWebView.evaluateJavascript(sc, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.d("PERFORMANCE", "processing time: " + s);
                    }
                });
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (mCount < urls.length) {
                    mWebView.loadUrl(urls[mCount++]);
                } else {
                    finish();
                }
            }
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        mWebView = (WebView) findViewById(R.id.myWebView);
        mWebView.setWebViewClient(getWvc());
        wvConfiguration(mWebView);
        mWebView.loadUrl(urls[mCount]);
    }
}
