ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* README.md
* build.xml
* keystore\
* keystore\SPEC_CPU.keystore
* keystore\shivam.keystore
* makefile
* proguard-project.txt

Replaced Jars with Dependencies:
--------------------------------
The importer recognized the following .jar files as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the .jar file in your project was of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the jar replacement in the import wizard and try again:

android-support-v4.jar => com.android.support:support-v4:26.0.0-alpha1

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => app\src\main\AndroidManifest.xml
* assets\ => app\src\main\assets\
* jni\ => app\src\main\jni\
* libs\armeabi\libbzip2.so => app\src\main\jniLibs\armeabi\libbzip2.so
* libs\armeabi\libbzip2O1.so => app\src\main\jniLibs\armeabi\libbzip2O1.so
* libs\armeabi\libbzip2O2.so => app\src\main\jniLibs\armeabi\libbzip2O2.so
* libs\armeabi\libbzip2O3.so => app\src\main\jniLibs\armeabi\libbzip2O3.so
* libs\armeabi\libbzip2O3F.so => app\src\main\jniLibs\armeabi\libbzip2O3F.so
* libs\armeabi\libcrafty.so => app\src\main\jniLibs\armeabi\libcrafty.so
* libs\armeabi\libcraftyO1.so => app\src\main\jniLibs\armeabi\libcraftyO1.so
* libs\armeabi\libcraftyO2.so => app\src\main\jniLibs\armeabi\libcraftyO2.so
* libs\armeabi\libcraftyO3.so => app\src\main\jniLibs\armeabi\libcraftyO3.so
* libs\armeabi\libcraftyO3F.so => app\src\main\jniLibs\armeabi\libcraftyO3F.so
* libs\armeabi\libgcc.so => app\src\main\jniLibs\armeabi\libgcc.so
* libs\armeabi\libgccO1.so => app\src\main\jniLibs\armeabi\libgccO1.so
* libs\armeabi\libgccO2.so => app\src\main\jniLibs\armeabi\libgccO2.so
* libs\armeabi\libgccO3.so => app\src\main\jniLibs\armeabi\libgccO3.so
* libs\armeabi\libgccO3F.so => app\src\main\jniLibs\armeabi\libgccO3F.so
* libs\armeabi\libgzip.so => app\src\main\jniLibs\armeabi\libgzip.so
* libs\armeabi\libgzipO1.so => app\src\main\jniLibs\armeabi\libgzipO1.so
* libs\armeabi\libgzipO2.so => app\src\main\jniLibs\armeabi\libgzipO2.so
* libs\armeabi\libgzipO3.so => app\src\main\jniLibs\armeabi\libgzipO3.so
* libs\armeabi\libgzipO3F.so => app\src\main\jniLibs\armeabi\libgzipO3F.so
* libs\armeabi\libmcf.so => app\src\main\jniLibs\armeabi\libmcf.so
* libs\armeabi\libmcfO1.so => app\src\main\jniLibs\armeabi\libmcfO1.so
* libs\armeabi\libmcfO2.so => app\src\main\jniLibs\armeabi\libmcfO2.so
* libs\armeabi\libmcfO3.so => app\src\main\jniLibs\armeabi\libmcfO3.so
* libs\armeabi\libmcfO3F.so => app\src\main\jniLibs\armeabi\libmcfO3F.so
* libs\armeabi\libparser.so => app\src\main\jniLibs\armeabi\libparser.so
* libs\armeabi\libparserO1.so => app\src\main\jniLibs\armeabi\libparserO1.so
* libs\armeabi\libparserO2.so => app\src\main\jniLibs\armeabi\libparserO2.so
* libs\armeabi\libparserO3.so => app\src\main\jniLibs\armeabi\libparserO3.so
* libs\armeabi\libparserO3F.so => app\src\main\jniLibs\armeabi\libparserO3F.so
* libs\armeabi\libtwolf.so => app\src\main\jniLibs\armeabi\libtwolf.so
* libs\armeabi\libtwolfO1.so => app\src\main\jniLibs\armeabi\libtwolfO1.so
* libs\armeabi\libtwolfO2.so => app\src\main\jniLibs\armeabi\libtwolfO2.so
* libs\armeabi\libtwolfO3.so => app\src\main\jniLibs\armeabi\libtwolfO3.so
* libs\armeabi\libtwolfO3F.so => app\src\main\jniLibs\armeabi\libtwolfO3F.so
* libs\armeabi\libvortex.so => app\src\main\jniLibs\armeabi\libvortex.so
* libs\armeabi\libvortexO1.so => app\src\main\jniLibs\armeabi\libvortexO1.so
* libs\armeabi\libvortexO2.so => app\src\main\jniLibs\armeabi\libvortexO2.so
* libs\armeabi\libvortexO3.so => app\src\main\jniLibs\armeabi\libvortexO3.so
* libs\armeabi\libvortexO3F.so => app\src\main\jniLibs\armeabi\libvortexO3F.so
* libs\armeabi\libvpr.so => app\src\main\jniLibs\armeabi\libvpr.so
* libs\armeabi\libvprO1.so => app\src\main\jniLibs\armeabi\libvprO1.so
* libs\armeabi\libvprO2.so => app\src\main\jniLibs\armeabi\libvprO2.so
* libs\armeabi\libvprO3.so => app\src\main\jniLibs\armeabi\libvprO3.so
* libs\armeabi\libvprO3F.so => app\src\main\jniLibs\armeabi\libvprO3F.so
* res\ => app\src\main\res\
* src\ => app\src\main\java\
* src\._.DS_Store => app\src\main\resources\._.DS_Store
* src\.DS_Store => app\src\main\resources\.DS_Store
* src\com\._.DS_Store => app\src\main\resources\com\._.DS_Store
* src\com\.DS_Store => app\src\main\resources\com\.DS_Store
* src\com\arieslabs\assetbridge\Assetbridge.java~ => app\src\main\resources\com\arieslabs\assetbridge\Assetbridge.java~
* src\com\example\._.DS_Store => app\src\main\resources\com\example\._.DS_Store
* src\com\example\.DS_Store => app\src\main\resources\com\example\.DS_Store
* src\com\example\hellojni\Result.java~ => app\src\main\resources\com\example\hellojni\Result.java~
* src\com\example\hellojni\Run.java~ => app\src\main\resources\com\example\hellojni\Run.java~

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
