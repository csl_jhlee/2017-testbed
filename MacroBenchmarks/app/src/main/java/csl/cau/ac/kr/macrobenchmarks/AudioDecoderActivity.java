package csl.cau.ac.kr.macrobenchmarks;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by user on 2017-10-24.
 */

public class AudioDecoderActivity extends AppCompatActivity {
    protected static AudioDecoderThread mAudioDecoder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_decoder);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment()).commit();
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        private static final String FILE_PATH = Environment.getExternalStorageDirectory() + "/Download/audiovorbis.oga";

        public PlaceholderFragment() {
            mAudioDecoder = new AudioDecoderThread();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_audio_decoder, container, false);

            final Button btn = (Button) rootView.findViewById(R.id.play);
            btn.setText("Vorbis Audio Decoder Benchmark");

            if (mAudioDecoder != null) {
                if (mAudioDecoder.init(FILE_PATH)) {
                    mAudioDecoder.start();
                    new Thread(new Runnable() {
                        public void run() {
                            try {
                                mAudioDecoder.join();
                                getActivity().finish();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();
                } else {
                    mAudioDecoder = null;
                }
            }

            return rootView;
        }
    }
}
