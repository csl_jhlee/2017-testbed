package csl.cau.ac.kr.macrobenchmarks;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    public final static int REQ_VIDEO = 0;
    public final static int REQ_AUDIO = 1;
    public final static int REQ_WEB   = 2;

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        verifyStoragePermissions(this);

        Intent intent = new Intent(this, VideoDecoderActivity.class);
        startActivityForResult(intent, REQ_VIDEO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case REQ_VIDEO:
                //you just got back from activity B - deal with resultCode
                //use data.getExtra(...) to retrieve the returned data
                Intent intent = new Intent(this, AudioDecoderActivity.class);
                startActivityForResult(intent, REQ_AUDIO);
                break;
            case REQ_AUDIO:
                Intent intent2 = new Intent(this, WebViewActivity.class);
                startActivityForResult(intent2, REQ_WEB);
                break;
            case REQ_WEB:
                //you just got back from activity C - deal with resultCode
                break;
        }
    }

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }
}
